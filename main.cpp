//
// Created by raleigh on 4/20/19.
//

#include <iostream>

#include "calculator.hpp"


int main() {

    Calculator<int> c;

    std::cout << c.add(1, 2) << std::endl;

}
