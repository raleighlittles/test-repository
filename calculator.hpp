//
// Created by raleigh on 4/20/19.
//

#ifndef TEST_REPOSITORY_CALCULATOR_H
#define TEST_REPOSITORY_CALCULATOR_H

template<class Type>
class Calculator {

public:
    Type add(Type a, Type b) {
        return a + b;
    }

    Type subtract(Type a, Type b) {
        return a - b;
    }

    Type multiply(Type a, Type b) {
        return a * b;
    }

    Type divide(Type a, Type b) {
        return a / b;
    }

};

#endif //TEST_REPOSITORY_CALCULATOR_H
